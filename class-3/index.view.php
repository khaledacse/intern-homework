<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style>
		.container{
				width: 50%;
				margin: 0 11px;
		}
	</style>

</head>
<body>
	<div class= "container mt-3">
	<table class= "table table-bordered table-hover text-center">
		<tr class= "table-primary">
			<td>Name</td>
			<td>Age</td>
			<td>HTML</td>
			<td>PHP</td>
			<td>Grade</td>
		</tr>

			<?php foreach ($student_info as $student):?>
				<?php
					if ($student['php'] == false)
					continue; 
				 ?>
				<tr>
					<td><?=$student['name'];?></td>
					<td><?=$student['age'];?></td>
					<td><?=checker($student['html']);?></td>
					<td><?=checker($student['php']);?></td>
					<td><?=grade($student['grade']);?></td>
				</tr>
			<?php endforeach; ?>
	</table>
	</div>	
</body>
</html>