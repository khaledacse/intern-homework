<?php

class Result{
    
    public static function studnets(){

        return $students = [
            [
                'name'  =>  'Khaleda Akter',
                'age'   =>  20,
                'html'  =>  true,
                'php'   =>  true,
                'grade' =>  4
            ],
            [
                'name'  =>  'MD. ABBAS UDDIN',
                'age'   =>  24,
                'html'  =>  true,
                'php'   =>  true,
                'grade' =>  3.75
            ],
            [
                'name'  =>  'Anower Ullah',
                'age'   =>  22,
                'html'  =>  true,
                'php'   =>  false,
                'grade' =>  1.9
            ],
            [
                'name'  =>  'Bijoy kumar',
                'age'   =>  28,
                'html'  =>  false,
                'php'   =>  true,
                'grade' =>  4.00
            ],
            [
                'name'  =>  'MD. ABBAS UDDIN',
                'age'   =>  24,
                'html'  =>  true,
                'php'   =>  true,
                'grade' =>  5
            ]
        ];
    }

    public static function grade($data){
        if($data >= 0.00 && $data <= 4.00){
            if ($data == 4) {
                $g = 'A+';
            } else if($data >= 3.75) {
                $g = 'A';
            } else if($data >= 3.50) {
                $g = 'A-';
            } else if($data >= 3.25) {
                $g = 'B+';
            } else if($data >= 3.00) {
                $g = 'B';
            } else if($data >= 2.75) {
                $g = 'B-';
            } else if($data >= 2.50) {
                $g = 'C+';
            } else if($data >= 2.25) {
                $g = 'C';
            } else if($data >= 2.00) {
                $g = 'D';
            } else if($data >= 0.00) {
                $g = 'F';
            }
        }else{
            $g = "Wrong Input";
        }
        return $g;
    }
    public static function checker($data){
        return $data == true ? '&#10003;' : '&#10005;';
    }
    public static function valided_data($grade){
        if($grade < 2.00 || $grade > 4.00){
            $color = 'bg-danger text-white';
        }  
        else if ($grade == 4){ 
            $color = 'bg-success text-white';
        }
        else{
            $color = '';
        }  
        return $color;
    }
}

require_once "Result.view.php";

?>
