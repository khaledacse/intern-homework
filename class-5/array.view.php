<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style>
        .container{
            width: 500px;
            margin-top: 50px;
        }
    </style>
</head>
<body>
    <div class="container">
        <table class="table table-bordered border-primary text-center">
            <thead class="table-primary">
                <tr>
                    <th>Name</th>
                    <th>Roll</th>
                    <th>REG</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                foreach($students as $student):
            ?>
                <tr>
                    <td><?= $student['name']; ?></td>
                    <td><?= $student['roll']; ?></td>
                    <td><?= $student['reg']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</body>
</html>